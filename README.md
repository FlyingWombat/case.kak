## case.kak
Provides commands for changing compound word case conventions.
In general, the plugin works by changing each selection to snake\_case,
then from snake\_case to the target case.

Note: Most of the commands in this plugin will write to the mark (^) register,
the one execption being case-select-joints.

### Installation
Clone this repo into your `autoload` folder,
or install using your favorite plugin manager.

### Usage
Here is the user mode added by case.kak:
```
declare-user-mode case
map global case u '~' -docstring "upper"
map global case l '`' -docstring "lower"
map global case i '<a-`>' -docstring "invert"
map global case s       ': case-to-snake<ret>'  -docstring "snake_case"
map global case d       ': case-to-dash<ret>'   -docstring "dash-case"
map global case .       ': case-to-dot<ret>'    -docstring "dot.case"
map global case <space> ': case-to-space<ret>'  -docstring "space case"
map global case t       ': case-to-title<ret>'  -docstring "Title Case"
map global case c       ': case-to-camel<ret>'  -docstring "camelCase"
map global case p       ': case-to-pascal<ret>' -docstring "PascalCase"
map global case j       ': case-select-joints<ret>' -docstring "select joints"
map global case J       ': case-unify-joints<ret>' -docstring "unify joints"
map global case w       ': case-select-words<ret>' -docstring "select words"
```

Suggested mapping:
```
map global normal '`' ': enter-user-mode case<ret>'
```
