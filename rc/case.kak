# Case.kak
# provides commands for changing compound word case conventions
# In general, the plugin works by changing each selection to snake_case,
# then from snake_case to the target case.
#
# Most of the commands in this plugin will write to the mark (^) register,
# the one execption being case-select-joints.

# regex expression to match compound word separators
# This will be used within other regex
declare-option str case_word_chars "A-Za-z0-9"


define-command -hidden -docstring "select compound word separators" \
case-select-separator %{
    execute-keys "_s[^%opt{case_word_chars}\n]<ret>"
}

define-command -hidden -docstring "select joints in camel/pascal case" \
case-select-camel %{
    execute-keys "s\B(?<lt>![A-Z])[A-Z]|\B[A-Z](?![A-Z])<ret>"
}


define-command -docstring "select joints in a compound word" \
case-select-joints %{
    evaluate-commands -itersel %{
        try %{ case-select-separator
        } catch %{ case-select-camel
        } catch %{ fail "%val{error}" }
    }
}

# the inverse of case-select-joints
define-command -docstring "select parts of a compound word" \
case-select-words %{
    evaluate-commands -itersel %{
        try %{
            execute-keys "_s\A[%opt{case_word_chars}]+\z<ret>"
            execute-keys "S\B(?<lt>![A-Z])[A-Z]|\B[A-Z](?![A-Z])<ret>"
        } catch %{ execute-keys "_S[^%opt{case_word_chars}\n]<ret>"
        } catch %{ fail "%val{error}" }
    }
}

define-command -docstring "select and unify joints to _ (almost snake_case)" \
case-unify-joints %{
    execute-keys -save-regs '' 'Z'
    evaluate-commands -itersel %{
        try %{
            case-select-separator
            execute-keys "r_"
        } catch %{
            case-select-camel
            execute-keys "i_<esc>h"
        } catch %{ fail "%val{error}" }
    }
}

define-command -docstring "Convert selections to snake_case" \
case-to-snake %{
    case-unify-joints
    execute-keys "z`"
}

define-command -docstring "Convert selections to dash-case" \
case-to-dash %{
    case-unify-joints
    execute-keys "r-z`"
}

define-command -docstring "Convert selections to dot.case" \
case-to-dot %{
    case-unify-joints
    execute-keys "r.z`"
}

define-command -docstring "Convert selections to space case" \
case-to-space %{
    case-unify-joints
    execute-keys "r z`"
}

define-command -docstring "Convert selections to snake_case" \
case-to-title %{
    case-to-snake
    execute-keys -save-regs '' 'Z'
    case-select-separator
    execute-keys "r l~z<a-;>;~z"
    # first letter of selection is left out
    execute-keys "<a-:><a-;>H<a-:>"
}

define-command -docstring "Convert selections to camelCase" \
case-to-camel %{
    case-to-snake
    execute-keys -save-regs '' 'Z'
    case-select-separator
    execute-keys "d~z"
}

define-command -docstring "Convert selections to PascalCase" \
case-to-pascal %{
    case-to-snake
    execute-keys -save-regs '' 'Z'
    case-select-separator
    execute-keys "d~b;~z"
    # first letter of selection is left out
    execute-keys "<a-:><a-;>H<a-:>"
}


# Case Convert mode
declare-user-mode case
map global case u '~' -docstring "upper"
map global case l '`' -docstring "lower"
map global case i '<a-`>' -docstring "invert"
map global case s       ': case-to-snake<ret>'  -docstring "snake_case"
map global case d       ': case-to-dash<ret>'   -docstring "dash-case"
map global case .       ': case-to-dot<ret>'    -docstring "dot.case"
map global case <space> ': case-to-space<ret>'  -docstring "space case"
map global case t       ': case-to-title<ret>'  -docstring "Title Case"
map global case c       ': case-to-camel<ret>'  -docstring "camelCase"
map global case p       ': case-to-pascal<ret>' -docstring "PascalCase"
map global case j       ': case-select-joints<ret>' -docstring "select joints"
map global case J       ': case-unify-joints<ret>' -docstring "unify joints"
map global case w       ': case-select-words<ret>' -docstring "select words"


# Suggested mapping
# map global normal '`' ': enter-user-mode case<ret>'
